This project uses the premises of [readertray](https://code.google.com/p/readertray/) for GUI, and the goslate module for communication with Google Translate, and the clipboard module for python, allowing all the words of clipboard are translated

##Install Dependency #

```
pip install goslate
```
```
pip install clipboard
```
```
pip install pygtk
```
```
sudo apt-get install libwebkit-dev
```
```
sudo apt-get install xclip
```
### Install wx
* Install requirements (Linux):
- basic development things, change the last if you want to install to Python 3:
```
#!bash
sudo apt-get install dpkg-dev build-essential python2.7-dev
```
- some image libraries
```
sudo apt-get install libjpeg-dev libtiff-dev
```
- GTK 2
```
sudo apt-get install libgtk2.0-dev
```
- Media stuff
```
sudo apt-get install libsdl1.2-dev libgstreamer-plugins-base0.10-dev
```
* Install wxPython-Phoenix (Linux):

```
sudo pip install --upgrade --trusted-host wxpython.org --pre -f http://wxpython.org/Phoenix/snapshot-builds/ wxPython_Phoenix 
```

* Install wxPython-Phoenix (Windows, use the appropriate script folder):
```
C:\python34\scripts\pip.exe install --upgrade  --trusted-host wxpython.org --pre -f http://wxpython.org/Phoenix/snapshot-builds/ wxPython_Phoenix 
```
### run ###

python main.py

### Windows Startup ###
Move file start_translate.bat to : %AppData%\Microsoft\Windows\Start Menu\Programs\Startup
