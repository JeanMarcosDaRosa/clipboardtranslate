#!/usr/bin/env python

import goslate
import clipboard
import time
import wx
from wx import adv
import os, sys
from os import system
import platform

__author__ = 'Jean Marcos da Rosa >> jeanmarcosdarosa@gmail.com'

def file(path, mode="r"):
    f = os.path.join(os.path.dirname(sys._getframe(1).f_code.co_filename), path)
    if mode != "p":
        f = __builtins__["file"](f, mode)
    return f


class Icon(adv.TaskBarIcon):

    def __init__(self, menu):
        self.locale = wx.Locale(wx.LANGUAGE_ENGLISH)
        adv.TaskBarIcon.__init__(self)
        self.menu = menu

        self.Bind(adv.EVT_TASKBAR_LEFT_DOWN, self.click)
        self.Bind(adv.EVT_TASKBAR_RIGHT_DOWN, self.click)
        self.Bind(wx.EVT_MENU, self.select)

        self.states = {
            "on": wx.Icon(file("dat/reader_new.png", "p"), wx.BITMAP_TYPE_PNG),
            "off": wx.Icon(file("dat/reader_empty.png", "p"), wx.BITMAP_TYPE_PNG)
        }
        self.setStatus("off")

    def click(self, event):
        menu = wx.Menu()
        for id, item in enumerate(self.menu):
            menu.Append(id, item[0])
        self.PopupMenu(menu)

    def select(self, event):
        self.menu[event.GetId()][1]()

    def setStatus(self, which):
        self.SetIcon(self.states[which])

    def close(self):
        self.Destroy()


class Popup(wx.Frame):
    
    def __init__(self):
        wx.Frame.__init__(self, None, -1, style=wx.NO_BORDER|wx.FRAME_NO_TASKBAR)
        self.padding = 12 # padding between edge, icon and text
        self.popped = 0 # the time popup was opened
        self.delay = 4 # time to leave the popup opened

        # platform specific hacks
        lines = 2
        lineHeight = wx.MemoryDC().GetTextExtent(" ")[1]
        if wx.Platform == "__WXGTK__":
            self.popup = wx.PopupWindow(self, -1)
        elif wx.Platform == "__WXMSW__":
            self.popup = self
            lineHeight -= 3
        elif wx.Platform == "__WXMAC__":
            self.popup = self

        self.popup.SetSize((250, (lineHeight * (lines + 1)) + (self.padding * 2)))
        self.panel = wx.Panel(self.popup, -1, size=self.popup.GetSize())

        # popup's click handler
        self.panel.Bind(wx.EVT_LEFT_DOWN, self.click)

        # popup's logo
        self.logo = wx.Bitmap(file("dat/reader_large.png", "p"))
        wx.StaticBitmap(self.panel, -1, pos=(self.padding, self.padding)).SetBitmap(self.logo)

        # main timer routine
        self.timer = wx.Timer(self, -1)
        self.Bind(wx.EVT_TIMER, self.main, self.timer)
        self.timer.Start(500)


    def main(self, event):
        if self.focused():
            # maintain opened state if focused
            self.popped = time.time()
        elif self.opened() and self.popped + self.delay < time.time():
            # hide the popup once delay is reached
            self.hide()


    def click(self, event):
        self.popped = 0
        self.hide()


    def show(self, text):
        # create new text
        if hasattr(self, "text"):
            self.text.Destroy()
        popupSize = self.popup.GetSize()
        logoSize = self.logo.GetSize()
        self.text = wx.StaticText(self.panel, -1, text)
        self.text.Bind(wx.EVT_LEFT_DOWN, self.click)
        self.text.Move((logoSize.width + (self.padding * 2), self.padding))
        self.text.SetSize((
            popupSize.width - logoSize.width - (self.padding * 3),
            popupSize.height - (self.padding * 2)
        ))
        # animate the popup
        screen = wx.GetClientDisplayRect()
        self.popup.Show()
        self.popup.SetFocus()
        for i in range(1, popupSize.height + 1):
            self.popup.Move((screen.width - popupSize.width, screen.height - i))
            self.popup.SetTransparent(int(float(240) / popupSize.height * i))
            self.popup.Update()
            self.popup.Refresh()
            time.sleep(0.01)
        self.popped = time.time()
        

    def hide(self):
        self.popup.Hide()
        self.popped = 0


    def focused(self):
        mouse = wx.GetMousePosition()
        popup = self.popup.GetScreenRect()
        return (
            self.popped and
            mouse.x in range(popup.x, popup.x + popup.width)
            and mouse.y in range(popup.y, popup.y + popup.height)
        )


    def opened(self):
        return self.popped != 0


    def close(self):
        self.Destroy()


class Notifier(wx.App):

    def __init__(self):
        wx.App.__init__(self, redirect=0)

        menu = [
            ("Mostrar último", self.again),
            ("Sair", self.exit),
        ]

        self.icon = Icon(menu)
        self.popup = Popup()
        self.historico = {}
        self.last_item = None
        self.lang = 'pt-BR'
        self.gs = goslate.Goslate()
        timer = wx.Timer(self, -1)
        self.Bind(wx.EVT_TIMER, self.main, timer)
        timer.Start(500)
        self.MainLoop()


    def main(self, event):
        if not self.popup.opened():
            if len(self.historico) > 10:
                label = list(self.historico.keys())[0]
                del self.historico[label]
            text = clipboard.paste()
            if text!=None and 0 < len(text) < 80: #tamanho maximo da string
                if self.last_item!=text:
                    self.last_item=text
                    if text.encode('utf-8') in self.historico:
                        self.popup.show(self.historico[text.encode('utf-8')])
                        self.icon.setStatus('on')
                    else:
                        result = self.gs.translate(text, self.lang)
                        self.historico[text.encode('utf-8')] = result
                        self.popup.show(result.encode('utf-8'))
                        self.icon.setStatus('on')
                else:
                    self.icon.setStatus('off')


    def again(self):
        if not self.popup.opened() and self.last_item!=None:
            self.popup.show(self.last_item.encode('utf-8'))
            self.icon.setStatus('on')


    def exit(self):
        self.icon.close()
        self.popup.close()


notifier = Notifier()
if platform.system().lower() == 'windows':
    system("title Clipboard Translator 0.1")